package net.adrianoluis.code;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EPAMTechInterviewTest {

	private EPAMTechInterview test;

	@Before
	public void setUp() {
		test = new EPAMTechInterview();
	}

	@Test
	public void testSolve1() throws Throwable {
		final Map<Integer, Integer> expected = new HashMap<>();
		expected.put(5, 1);
		expected.put(4, 2);

		final Map<Integer, Integer> actual = test.solve(6, 0, 3, 5, 1, 4, 2);
		assertNotNull(actual);

		assertEquals(expected, actual);
	}

	@Test
	public void testSolve2() throws Throwable {
		final Map<Integer, Integer> expected = new HashMap<>();
		expected.put(3, 3);
		expected.put(5, 1);
		expected.put(4, 2);

		final Map<Integer, Integer> actual = test.solve(6, 0, 3, 5, 1, 4, 3, 2);
		assertNotNull(actual);

		assertEquals(expected, actual);
	}

	@Test
	public void testSolve3() throws Throwable {
		final Map<Integer, Integer> expected = new HashMap<>();
		expected.put(0, 9);
		expected.put(4, 5);
		expected.put(2, 7);
		expected.put(1, 8);

		final Map<Integer, Integer> actual = test.solve(9, 0, 4, 2, 6, 5, 5, 1, 2, 8, 7, 9, 0, 4, 5);
		assertNotNull(actual);

		assertEquals(expected, actual);
	}

	@Test
	public void testSolve4() throws Throwable {
		final Map<Integer, Integer> expected = new HashMap<>();
		expected.put(8, 0);
		expected.put(5, 3);
		expected.put(2, 6);
		expected.put(4, 4);

		final Map<Integer, Integer> actual = test.solve(8, 8, 5, 3, 4, 2, 2, 6, 8, 5, 0, 9, 1, 0, 4);
		assertNotNull(actual);

		assertEquals(expected, actual);
	}

	@Test
	public void testSolve5() throws Throwable {
		final Map<Integer, Integer> expected = new HashMap<>();
		expected.put(390, 123);
		expected.put(455, 58);
		expected.put(297, 216);
		expected.put(426, 87);
		expected.put(360, 153);
		expected.put(148, 365);
		expected.put(199, 314);
		expected.put(264, 249);
		expected.put(28, 485);
		expected.put(303, 210);
		expected.put(294, 219);
		expected.put(223, 290);
		expected.put(236, 277);
		expected.put(421, 92);
		expected.put(75, 438);
		expected.put(104, 409);
		expected.put(74, 439);
		expected.put(316, 197);
		expected.put(402, 111);
		expected.put(398, 115);
		expected.put(404, 109);
		expected.put(7, 506);
		expected.put(180, 333);
		expected.put(381, 132);
		expected.put(227, 286);
		expected.put(353, 160);
		expected.put(83, 430);
		expected.put(451, 62);
		expected.put(57, 456);
		expected.put(331, 182);
		expected.put(127, 386);
		expected.put(267, 246);
		expected.put(220, 293);
		expected.put(364, 149);
		expected.put(52, 461);
		expected.put(50, 463);
		expected.put(274, 239);
		expected.put(8, 505);
		expected.put(44, 469);
		expected.put(214, 299);
		expected.put(181, 332);
		expected.put(46, 467);
		expected.put(355, 158);
		expected.put(373, 140);
		expected.put(154, 359);
		expected.put(278, 235);
		expected.put(482, 31);
		expected.put(60, 453);
		expected.put(449, 64);
		expected.put(471, 42);
		expected.put(369, 144);
		expected.put(310, 203);
		expected.put(241, 272);
		expected.put(473, 40);
		expected.put(300, 213);
		expected.put(128, 385);
		expected.put(436, 77);
		expected.put(177, 336);
		expected.put(380, 133);
		expected.put(444, 69);
		expected.put(376, 137);
		expected.put(18, 495);
		expected.put(187, 326);
		expected.put(306, 207);
		expected.put(488, 25);
		expected.put(307, 206);
		expected.put(47, 466);
		expected.put(161, 352);
		expected.put(3, 510);
		expected.put(450, 63);
		expected.put(72, 441);
		expected.put(479, 34);
		expected.put(126, 387);
		expected.put(370, 143);
		expected.put(342, 171);
		expected.put(173, 340);
		expected.put(10, 503);
		expected.put(155, 358);
		expected.put(119, 394);
		expected.put(420, 93);
		expected.put(103, 410);
		expected.put(27, 486);
		expected.put(481, 32);
		expected.put(259, 254);
		expected.put(377, 136);
		expected.put(317, 196);
		expected.put(383, 130);
		expected.put(211, 302);
		expected.put(501, 12);
		expected.put(247, 266);
		expected.put(222, 291);
		expected.put(82, 431);
		expected.put(80, 433);
		expected.put(191, 322);
		expected.put(309, 204);
		expected.put(335, 178);
		expected.put(39, 474);
		expected.put(242, 271);
		expected.put(81, 432);
		expected.put(497, 16);
		expected.put(167, 346);
		expected.put(494, 19);
		expected.put(399, 114);
		expected.put(339, 174);
		expected.put(224, 289);
		expected.put(229, 284);
		expected.put(384, 129);
		expected.put(308, 205);
		expected.put(374, 139);
		expected.put(319, 194);
		expected.put(201, 312);
		expected.put(100, 413);
		expected.put(462, 51);
		expected.put(273, 240);
		expected.put(159, 354);
		expected.put(251, 262);
		expected.put(321, 192);
		expected.put(269, 244);
		expected.put(378, 135);
		expected.put(422, 91);
		expected.put(396, 117);
		expected.put(243, 270);
		expected.put(253, 260);
		expected.put(170, 343);
		expected.put(429, 84);
		expected.put(101, 412);
		expected.put(141, 372);
		expected.put(423, 90);
		expected.put(147, 366);
		expected.put(499, 14);
		expected.put(257, 256);
		expected.put(265, 248);
		expected.put(217, 296);
		expected.put(472, 41);
		expected.put(30, 483);
		expected.put(509, 4);
		expected.put(492, 21);
		expected.put(279, 234);
		expected.put(454, 59);
		expected.put(98, 415);
		expected.put(6, 507);
		expected.put(121, 392);
		expected.put(511, 2);
		expected.put(118, 395);
		expected.put(435, 78);
		expected.put(327, 186);
		expected.put(403, 110);
		expected.put(252, 261);
		expected.put(189, 324);
		expected.put(512, 1);
		expected.put(225, 288);
		expected.put(238, 275);
		expected.put(112, 401);
		expected.put(276, 237);
		expected.put(320, 193);
		expected.put(489, 24);
		expected.put(408, 105);
		expected.put(323, 190);
		expected.put(379, 134);
		expected.put(311, 202);
		expected.put(345, 168);
		expected.put(226, 287);
		expected.put(329, 184);
		expected.put(97, 416);
		expected.put(388, 125);
		expected.put(442, 71);
		expected.put(89, 424);
		expected.put(70, 443);
		expected.put(361, 152);
		expected.put(447, 66);
		expected.put(305, 208);
		expected.put(389, 124);
		expected.put(36, 477);
		expected.put(33, 480);
		expected.put(357, 156);
		expected.put(427, 86);
		expected.put(405, 108);
		expected.put(230, 283);
		expected.put(382, 131);
		expected.put(446, 67);
		expected.put(500, 13);
		expected.put(391, 122);
		expected.put(94, 419);
		expected.put(157, 356);
		expected.put(29, 484);
		expected.put(172, 341);
		expected.put(20, 493);
		expected.put(76, 437);
		expected.put(233, 280);
		expected.put(350, 163);
		expected.put(73, 440);
		expected.put(95, 418);
		expected.put(504, 9);
		expected.put(347, 166);
		expected.put(258, 255);
		expected.put(445, 68);
		expected.put(65, 448);
		expected.put(468, 45);
		expected.put(460, 53);
		expected.put(282, 231);
		expected.put(250, 263);
		expected.put(464, 49);
		expected.put(232, 281);
		expected.put(337, 176);
		expected.put(56, 457);
		expected.put(295, 218);
		expected.put(54, 459);
		expected.put(334, 179);
		expected.put(476, 37);
		expected.put(175, 338);
		expected.put(349, 164);
		expected.put(465, 48);
		expected.put(38, 475);
		expected.put(458, 55);
		expected.put(138, 375);
		expected.put(292, 221);
		expected.put(487, 26);
		expected.put(367, 146);
		expected.put(215, 298);
		expected.put(61, 452);
		expected.put(11, 502);
		expected.put(508, 5);
		expected.put(406, 107);
		expected.put(417, 96);
		expected.put(425, 88);
		expected.put(285, 228);
		expected.put(478, 35);
		expected.put(268, 245);
		expected.put(145, 368);
		expected.put(185, 328);
		expected.put(318, 195);
		expected.put(99, 414);
		expected.put(304, 209);
		expected.put(198, 315);
		expected.put(498, 15);
		expected.put(169, 344);
		expected.put(212, 301);
		expected.put(17, 496);
		expected.put(428, 85);
		expected.put(434, 79);
		expected.put(113, 400);
		expected.put(116, 397);
		expected.put(150, 363);
		expected.put(22, 491);
		expected.put(330, 183);
		expected.put(188, 325);
		expected.put(371, 142);
		expected.put(162, 351);
		expected.put(120, 393);
		expected.put(23, 490);
		expected.put(348, 165);
		expected.put(313, 200);
		expected.put(43, 470);
		expected.put(407, 106);
		expected.put(411, 102);
		expected.put(151, 362);

		final Map<Integer, Integer> actual = test.solve(513, 390, 708, 900, 455, 297, 894, 426, 714,
				866, 360, 148, 199, 633, 264, 28, 303, 294, 647, 674, 969, 223, 236, 513, 421, 996, 75, 964, 104, 74,
				316, 584, 402, 398, 404, 854, 731, 7, 180, 381, 860, 895, 227, 777, 353, 83, 881, 978, 451, 578, 57,
				613, 331, 819, 697, 540, 842, 873, 557, 127, 267, 824, 762, 220, 364, 595, 52, 961, 50, 626, 274, 946,
				8, 44, 214, 181, 46, 355, 373, 733, 154, 569, 278, 897, 546, 920, 998, 482, 62, 962, 955, 972, 830,
				60, 902, 855, 449, 471, 369, 780, 515, 310, 794, 241, 726, 938, 677, 473, 649, 300, 747, 793, 219,
				637, 128, 575, 436, 790, 721, 177, 676, 386, 380, 444, 376, 18, 187, 306, 488, 307, 939, 605, 813,
				594, 286, 541, 47, 745, 161, 672, 725, 3, 601, 450, 72, 744, 479, 126, 835, 789, 370, 838, 678, 342,
				871, 770, 173, 10, 155, 607, 909, 642, 788, 119, 805, 544, 652, 743, 420, 977, 519, 883, 103, 69, 599,
				666, 717, 27, 481, 259, 377, 317, 653, 791, 383, 211, 954, 706, 501, 247, 222, 651, 463, 82, 904, 137,
				624, 539, 849, 80, 191, 784, 309, 913, 737, 335, 39, 242, 797, 81, 709, 983, 549, 497, 551, 937, 903,
				506, 760, 167, 928, 494, 399, 339, 224, 229, 384, 308, 735, 374, 319, 823, 950, 530, 958, 201, 100,
				665, 462, 123, 273, 654, 159, 251, 618, 321, 130, 114, 543, 692, 727, 598, 269, 832, 378, 876, 31,
				153, 422, 396, 243, 723, 994, 809, 796, 253, 963, 170, 429, 992, 101, 531, 141, 566, 865, 423, 947,
				942, 739, 147, 868, 645, 93, 299, 244, 499, 257, 850, 875, 749, 129, 16, 265, 892, 217, 518, 517, 694,
				472, 30, 509, 635, 696, 684, 916, 492, 888, 901, 279, 880, 454, 98, 6, 503, 121, 511, 581, 118, 385,
				435, 667, 833, 135, 700, 668, 314, 592, 327, 953, 548, 403, 690, 859, 629, 252, 722, 189, 512, 322,
				527, 225, 763, 132, 158, 249, 246, 644, 238, 332, 112, 755, 276, 207, 934, 588, 884, 896, 320, 489,
				648, 469, 291, 533, 602, 611, 408, 783, 638, 213, 1, 323, 593, 379, 311, 986, 345, 608, 769, 603, 845,
				908, 392, 293, 728, 889, 21, 583, 111, 226, 671, 260, 358, 329, 97, 388, 196, 412, 149, 442, 89, 70,
				361, 447, 600, 630, 872, 713, 305, 438, 389, 36, 534, 14, 680, 821, 799, 33, 357, 911, 691, 427, 443,
				730, 572, 781, 405, 483, 4, 891, 84, 858, 230, 382, 609, 124, 235, 234, 538, 446, 500, 529, 878, 929,
				391, 757, 655, 395, 94, 157, 936, 208, 210, 29, 982, 172, 811, 573, 736, 712, 882, 20, 532, 76, 798,
				233, 350, 615, 853, 73, 95, 536, 688, 171, 610, 288, 58, 504, 105, 133, 768, 77, 480, 347, 960, 258,
				995, 237, 792, 985, 776, 248, 987, 981, 669, 720, 326, 590, 445, 682, 663, 204, 202, 432, 197, 980,
				168, 926, 424, 917, 1000, 65, 906, 468, 661, 707, 460, 282, 570, 250, 552, 160, 456, 464, 898, 439,
				587, 520, 857, 537, 45, 656, 814, 861, 232, 772, 64, 806, 337, 56, 623, 152, 952, 295, 42, 19, 701,
				606, 752, 545, 176, 54, 565, 415, 910, 941, 973, 742, 334, 410, 887, 535, 66, 753, 63, 640, 925, 354,
				476, 968, 175, 634, 949, 216, 984, 387, 349, 465, 266, 945, 38, 312, 71, 933, 751, 458, 826, 138, 571,
				764, 333, 870, 746, 441, 837, 166, 650, 359, 139, 542, 292, 271, 487, 970, 240, 831, 40, 367, 767,
				641, 87, 215, 254, 419, 522, 643, 61, 394, 759, 11, 771, 621, 561, 804, 664, 502, 912, 340, 474, 508,
				686, 670, 863, 579, 662, 956, 997, 143, 32, 406, 205, 867, 134, 879, 604, 685, 556, 591, 921, 886,
				417, 302, 425, 270, 136, 285, 716, 96, 885, 25, 841, 993, 567, 485, 179, 68, 478, 263, 704, 915, 268,
				145, 262, 9, 564, 255, 711, 41, 185, 2, 298, 562, 318, 338, 614, 807, 773, 931, 516, 812, 585, 774,
				836, 99, 414, 971, 486, 778, 283, 597, 49, 526, 754, 368, 738, 304, 918, 750, 775, 198, 498, 927, 169,
				289, 856, 659, 979, 787, 514, 67, 131, 53, 174, 107, 907, 475, 632, 272, 576, 457, 146, 15, 703, 818,
				109, 765, 914, 740, 848, 346, 890, 718, 732, 144, 212, 507, 17, 416, 452, 899, 815, 428, 86, 810, 344,
				675, 957, 715, 375, 782, 434, 636, 839, 113, 785, 116, 453, 79, 586, 559, 834, 761, 178, 563, 261,
				324, 874, 631, 352, 150, 35, 505, 22, 330, 397, 190, 844, 574, 461, 467, 852, 699, 343, 218, 620, 239,
				816, 817, 91, 228, 596, 418, 433, 617, 840, 658, 188, 287, 13, 193, 371, 341, 695, 162, 284, 120, 437,
				990, 568, 493, 547, 275, 976, 622, 989, 521, 905, 194, 869, 639, 628, 657, 90, 221, 919, 965, 413,
				491, 85, 991, 827, 786, 820, 23, 822, 555, 803, 365, 681, 431, 495, 125, 710, 808, 554, 948, 348, 301,
				356, 705, 851, 115, 748, 351, 560, 800, 51, 702, 372, 5, 589, 195, 142, 477, 923, 336, 245, 558, 689,
				313, 930, 256, 363, 922, 496, 24, 448, 315, 999, 163, 616, 156, 864, 951, 524, 466, 741, 328, 625,
				183, 893, 679, 550, 490, 553, 646, 280, 687, 724, 43, 627, 525, 734, 846, 140, 943, 932, 37, 683, 407,
				411, 523, 729, 698, 366, 693, 801, 758, 296, 400, 102, 660, 59, 151, 673, 186, 362, 974, 106, 203,
				802, 110, 164, 847, 944, 877, 470, 459, 766, 206, 719, 959, 231, 88, 108, 577, 290, 580, 281, 988,
				200, 582, 117, 184, 779, 277, 440, 828, 393, 182, 975, 26, 165, 484, 966, 843, 192, 940, 510, 862,
				430, 209, 401, 48, 612, 55, 325, 924, 34, 92, 756, 122, 78, 12, 409, 935, 619, 967, 829, 795, 825,
				528);
		assertNotNull(actual);

		assertEquals(expected, actual);
	}
}
